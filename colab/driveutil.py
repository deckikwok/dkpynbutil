from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from google.colab import auth
from oauth2client.client import GoogleCredentials

global driveService

def authDrive():
    global driveService
    auth.authenticate_user()
    gauth = GoogleAuth()
    gauth.credentials = GoogleCredentials.get_application_default()
    driveService = GoogleDrive(gauth)


def getFromDriveAndSaveFileLocally(filename, savedFilePath):
    global driveService
    # fixme: more than one same file?
    fid = driveService.ListFile({'q': "title='" + filename + "'"}).GetList()[0]['id']
    print('whats fid ', fid)

    f = driveService.CreateFile({'id': fid})
    print('whats f ', f)

    # download as filename - stated in param - into local drive
    f.GetContentFile(savedFilePath)


def saveIntoDrive(blobcontent, title):
    global driveService
    drive_file = driveService.CreateFile({'title': title})
    drive_file.SetContentFile(blobcontent)
    drive_file.Upload()  # Upload the file.
    print('title: %s, id: %s' % (drive_file['title'], drive_file['id']))
