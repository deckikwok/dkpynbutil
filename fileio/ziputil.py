from zipfile import ZipFile

def unzipFileToDirectory(zipFilePath, extractDirPath):
    print('unzipping file..')

    zip_ref = ZipFile(zipFilePath, 'r')
    zip_ref.extractall(extractDirPath)
    zip_ref.close()

    print('all files unzipped')