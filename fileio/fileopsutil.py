import os

def createDirIfNotExist (dirPath):
    if (not os.path.exists(dirPath)):
        os.mkdir(dirPath)